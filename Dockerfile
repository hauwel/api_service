FROM python:3.10

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

WORKDIR /home/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN ~/.poetry/bin/poetry config virtualenvs.create false
COPY poetry.lock pyproject.toml ./
RUN ~/.poetry/bin/poetry install --no-interaction

COPY . .

CMD ["./scripts/docker_entrypoint.sh"]
