#!/usr/bin/env sh

while true
do
      read -r -p "ALERT! This will remove ALL DOCKER ASSETS. Are You Sure? [Y/n] " input

      case $input in
            [yY][eE][sS]|[yY])
                  echo "Yes"
                  break
                  ;;
            [nN][oO]|[nN])
                  echo "No"
                  exit 0
                  ;;
            *)
                  echo "Invalid input..."
                  ;;
      esac
done

docker container prune -f
docker image prune -af
docker volume prune -f

while true
do
      read -r -p "Should I also remove all intermediate images? [Y/n] " input

      case $input in
            [yY][eE][sS]|[yY])
                  echo "Yes"
                  break
                  ;;
            [nN][oO]|[nN])
                  echo "No"
                  exit 0
                  ;;
            *)
                  echo "Invalid input..."
                  ;;
      esac
done

docker builder prune -f
