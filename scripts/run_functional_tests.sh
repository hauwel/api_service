#!/usr/bin/env sh

./scripts/run_migrations.sh && \
pytest --asyncio-mode=auto functional_tests
