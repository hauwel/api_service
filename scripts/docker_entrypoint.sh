#!/usr/bin/env sh

./scripts/run_migrations.sh && \
uvicorn word_counter_service.web_api_service:app --host=0.0.0.0 --port=80 --reload --proxy-headers --reload-include=config.env
