#!/usr/bin/env sh

./scripts/run_migrations.sh && \
pytest --asyncio-mode=auto word_counter_service/tests
