#!/usr/bin/env sh

# entry point for bitbucket pipelines

pip install pre-commit && \
pre-commit run --all-files
