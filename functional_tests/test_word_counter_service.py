import http

import pytest
from httpx import AsyncClient
from loguru import logger

from word_counter_service.serializers import JobStatus
from word_counter_service.web_api_service import app


@pytest.mark.asyncio
@logger.catch
async def test_index_page_available():
    async with AsyncClient(app=app, base_url="http://test") as aclient:
        response = await aclient.get("/")
    assert response.status_code == 200
    assert response.json() == "Welcome to the Word Count Web API"


@pytest.mark.asyncio
@logger.catch
async def test_job_create():
    http_url = "https://example.com"
    async with AsyncClient(app=app, base_url="http://test") as aclient:

        # Get list of all jobs
        response = await aclient.get("/jobs/", follow_redirects=True)
        assert response.status_code == http.HTTPStatus.OK
        initial_jobs_count = len(response.json())

        # Create a job to parse http_url
        response = await aclient.post(
            "/jobs", json={"http_url": http_url}, follow_redirects=True
        )
        assert response.status_code == http.HTTPStatus.CREATED
        assert response.json()["http_url"] == http_url
        assert response.json()["status"] == JobStatus.created

        # Query the job by its id
        job_id = response.json()["id"]
        response = await aclient.get(f"/jobs/{job_id}", follow_redirects=True)
        assert response.status_code == http.HTTPStatus.OK
        assert response.json()["http_url"] == http_url
        assert response.json()["status"] == JobStatus.ready
        assert response.json()["result"]
        assert response.json()["error"] is None

        # Check final list of all jobs has been incremented
        response = await aclient.get("/jobs/", follow_redirects=True)
        assert response.status_code == http.HTTPStatus.OK
        final_jobs_count = len(response.json())
        assert initial_jobs_count == final_jobs_count - 1
