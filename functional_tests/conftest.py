import pytest
from httpx import AsyncClient

from word_counter_service.web_api_service import app


@pytest.mark.asyncio
@pytest.fixture(autouse=True)
async def async_client():
    async with AsyncClient(app=app) as aclient:
        yield aclient
