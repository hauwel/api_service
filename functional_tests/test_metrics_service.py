import pytest
from httpx import AsyncClient
from loguru import logger

from word_counter_service.web_api_service import app


@pytest.mark.asyncio
@logger.catch
async def test_index_page_available():
    async with AsyncClient(app=app, base_url="http://test") as aclient:
        response = await aclient.get("/metrics")
    assert response.status_code == 200
