[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

# Simple Web API #

This is simple web API to count words on a web page.

It is designed around concept of Jobs.

## Table Of Contents ##
* [How to run the service](#how-to-run-the-service)
* [Brief API description](#brief-api-description)
* [System Components](#system-components)
* [Architecture](#architecture)
* [Typical workflow example](#typical-workflow-example)
* [OpenAPI Documentation](#openapi-documentation)
* [Health Status Checks](#health-status-checks)
* [Project Structure Details](#project-structure-details)
* [Configuration](#configuration)
* [Multilanguage word count support](#multilanguage-word-count-support)
* [How to run tests](#how-to-run-tests)
* [How to get interactive console for the service container](#how-to-get-interactive-console-for-the-service-container)
* [Clean docker assets](#clean-docker-assets)
* [Limitations](#limitations)
* [Technologies motivation](#technologies-motivation)


### How to run the service ###

0. Check you have installed Docker,
[Docker](https://docs.docker.com/engine/install/)
[Docker Compose V2](https://docs.docker.com/compose/cli-command/#installing-compose-v2)
and [git](https://git-scm.com/downloads).

1. Clone the repository

```shell
git clone git@bitbucket.org:hauwel/api_service.git
```

2. Run the service using provided script
```shell
./run_service.sh
```

At this point you should be able to access URLs at [http://localhost:8000](http://localhost:8000).

For the very first run it might fail with an error like
`ConnectionRefusedError: [Errno 111] Connect call failed ('172.20.0.2', 5432)`
This is because DB has not started yet and rerunning `./run_service.sh` should help.

### Brief API description ###
This service exposes the following RESTFull API:

* `GET /jobs` to fetch all jobs available. Pager limits are configured (see [Configuration](#configuration))
* `POST /jobs` to create new job
* `GET /jobs/{id}` to fetch representation of a particular job
* `GET /metrics` URL with default prometheus metrics used as health status (see [Health Status Checks](#health-status-checks))
* `GET /index` simple welcome message

### System Components ###

* [http://localhost:8000/](http://localhost:8000/) Web Service
* [http://localhost:8000/docs](http://localhost:8000/docs)` Web Service OpenAPI
* [http://localhost:8000/redoc](http://localhost:8000/redoc) Another Web Service OpenAPI
* [http://localhost:9090](http://localhost:9090) Prometheus Monitoring UI
* [http://localhost:5432](http://localhost:5432) Postgres DB


### Architecture ###
A client makes a request to the service using RESTfull (HTTP) API.

Web service starts asynchronous job and redirects the client to the URL with the job's status and results.
Job result is not instantly available as the job does two tasks in the background:
queries provided URL and counts the words on the URL reply.

![Diagram](_doc_images/Diagram.svg)

[Diagram source](https://excalidraw.com/#json=qhZyllN_g0RLd_lauSjXY,j01faO9i0-iYLHSvbRFEZg)

### Typical workflow example: ###
#### 1. Client makes `POST /jobs` to initiate a web processing job. ####

This request only accepts single parameter `http_url` as JSON body.
```json
{"http_url": "https://example.com"}
```
The response will contain the job ID for further reference.

The response has the following JSON schema:
```json
{
  "id": "integer",
  "http_url": "string",
  "status": "created",  // initial status
  "error": "string",
  "result": "string"
}
```

#### 2. Using the Job id the client fetch the Job status

`GET /jobs/{job_id}` will return JSON with job representation.


Where job statuses are: `"created"`, `"in_progress"`, `"ready"` or `"error"`


### OpenAPI Documentation ###

OpenAPI documentation at [http://localhost:8000/docs](http://localhost:8000/docs) could be used for hands on testing.

ReDoc format available [http://localhost:8000/redoc](http://localhost:8000/redoc)

### Health Status Checks ###

The service is instrumented with `prometheus` metrics. Prometheus itself is available
at [http://localhost:9090](http://localhost:9090)

Simple gauge to monitor whether the service is up
[http://localhost:9090/graph?g0.expr=up%7Bjob%3D%22api_service%22%7D&g0.tab=0&g0.stacked=1&g0.show_exemplars=0&g0.range_input=1h](http://localhost:9090/graph?g0.expr=up%7Bjob%3D%22api_service%22%7D&g0.tab=0&g0.stacked=1&g0.show_exemplars=0&g0.range_input=1h)
### Project Structure Details ###

It is designed to be run in Docker containers.
There are several script to save typing under `./scripts` directory.

This project utilizes `pre-commit` checks and modify files with fixes automatically.

### Configuration ###

This application uses configuration technique from Starlette.
It recognises environment variables and `./config.env` file at the root of the repository.

The only required parameter is `DATABASE_URL`. The other have defaults but still present in the
`config.env` for visibility.

Parameters are mostly self-explanatory:

`DATABASE_URL`: Database URL

`DEBUG`: Enable debugging logging level.

`BEAUTIFULSOUP_PARSER`: Configuration for 3rd party `Beautifulsoup` library. If not set it is up to the library default.

`NON_WORD_REGEXP`: Regexp to split lines into words.

`DEFAULT_PAGE_LIMIT`: API response objects limit.

### Multilanguage word count support ###

Due to limitation of python regular expressions module `re` used for splitting data into words
the app is currently recognizes only english words. It is possible however to customize regular
expression provided as configuration parameter `NON_WORD_REGEXP`. See [Configuration](#configuration).

### How to run tests ###
Provided shell script should run two docker compose services `unit-test-api-service` `functional-test-api-service`

```shell
./run_tests.sh
```

Test database is created with 'test_{uuid4}' prefix to configured one.
Then all migrations are applied to the test database to check they are valid.

### How to get interactive console for the service container ###
```shell
./run_interactive_console.sh
```

### Clean docker assets ###

Handy interactive script which will ask about what to clean and what to keep
```shell
./stack_prune.sh
```

### Limitations ###

* Functional tests assume that internet is available and example.com site is up.


### Technologies and motivation ###

#### Asynchronous Python ####
Where possible make code asynchronous for the best CPU utilization.

#### FastAPI Async Python Framework [https://fastapi.tiangolo.com](https://fastapi.tiangolo.com)
Clean approach to code comes with included background tasks, automatic OpenAPI schema generation, seamless integration
with Pydantic, flexible for any ORM with the fastest benchmarks.

#### SQLAlchemy ####
Mighty ORM which comprises a lot of features from many SQL dialects. Mature and proven in production for many years.
Asynchronous DB access features were waiting for a long time is available now.

#### Alembic ####
SQLAlchemy sibling for managing migration with flexible migration generation mechanism giving freedom writing DB schema
and data migrations logic.

#### Pydantic ####
Simple fast serialization and validation library. Popular for these reasons. Great integrations with almost any python
web framework and ORM.

#### pytest ####
Python tests runner with well though testing design patterns.

#### poetry ####
Support for development environments. Two-staged configuration with project.toml and lock file facilitates better
package management (in comparison with pip). Clean and well-designed CLI.

#### pre-commit ####
Automated code quality checks usefull as pre-commit checks and as stand-alone formatter.

#### Prometheus ####
Simple fast monitoring system. One of the de-facto standard for metrics processing.

#### Docker. Docker Compose V2 ####
Application isolation. Needless to say it is now works on all popular OS. Lighter than VMs.
Reproducible sharable assets.

#### Bash scripts ####
To automate simple logic inside linux containers as de-facto preinstalled shell.

#### Ubuntu ####
User-friendly linux operating system with familiar set of management tools for package management.

#### excalidraw.com ####
Great sketching tool allowing blazing fast map ideas to an image.
