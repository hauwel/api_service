import re
from collections import Counter
from typing import AsyncGenerator

import requests
from bs4 import BeautifulSoup
from pydantic import HttpUrl
from sqlalchemy.ext.asyncio import AsyncSession

from word_counter_service import logger, settings
from word_counter_service.db.crud_jobs import update
from word_counter_service.models import Job
from word_counter_service.serializers import JobStatus, JobUpdate

__all__: list[str] = [
    "process",
]


@logger.catch
async def process(
    *,
    http_url: HttpUrl,
    job: Job,
    async_db_session: AsyncGenerator | AsyncSession,
) -> None:
    logger.debug(f"Begin processing {http_url}")
    await set_job_status(
        job=job,
        new_status=JobStatus.in_progress,
        async_db_session=async_db_session,
    )
    r = requests.get(http_url)
    html = BeautifulSoup(r.content, settings.BEAUTIFULSOUP_PARSER)
    wc = count_words(text=html.get_text(" "))
    await set_job_result(
        job_id=job.id,
        result=wc,
        async_db_session=async_db_session,
    )


def count_words(*, text: str):
    words = re.split(settings.NON_WORD_REGEXP, text, flags=re.UNICODE)
    lower_case_words = [w.lower() for w in words]
    return Counter(lower_case_words)


@logger.catch
async def set_job_status(
    job: Job,
    new_status: JobStatus,
    async_db_session: AsyncGenerator | AsyncSession,
) -> None:
    job_update: JobUpdate = JobUpdate.from_orm(job)
    job_update.status = new_status
    logger.debug(f"Setting {job} status: {new_status}")
    await update(job_update=job_update, async_db_session=async_db_session)
    await async_db_session.refresh(job)


@logger.catch
async def set_job_result(
    *,
    job_id: int,
    result: dict,
    async_db_session: AsyncGenerator | AsyncSession,
) -> None:
    job_update = JobUpdate(
        id=job_id,
        result=str(result),
        status=JobStatus.ready,
        error=None,
    )
    logger.debug(f"Setting job with {job_id} result: {result}")
    await update(job_update=job_update, async_db_session=async_db_session)
