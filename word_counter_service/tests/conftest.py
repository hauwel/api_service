from asyncio import current_task

import pytest
from sqlalchemy.ext.asyncio import (
    AsyncSession,
    async_scoped_session,
    create_async_engine,
)
from sqlalchemy.orm import sessionmaker

from word_counter_service.settings import DATABASE_URL


@pytest.fixture(autouse=True)
async def async_db_session():
    async_engine = create_async_engine(str(DATABASE_URL), echo=True)

    async_session_factory = sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=async_engine,
        class_=AsyncSession,
    )
    AsyncScopedSession = async_scoped_session(
        async_session_factory, scopefunc=current_task
    )
    async with AsyncScopedSession() as local_session:
        try:
            yield local_session
        finally:
            await local_session.close()
