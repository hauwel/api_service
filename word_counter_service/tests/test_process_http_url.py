from precisely import assert_that, mapping_includes

from word_counter_service.tasks import process_http_url


def test_sample_web_page():
    expected_word_count = {
        "tell": 2,
        "the": 1,
        "audience": 1,
        "what": 2,
        "you're": 1,
        "going": 1,
        "to": 1,
        "say": 2,
        "it": 1,
        "then": 1,
        "them": 1,
        "you've": 1,
        "said": 1,
    }
    result = process_http_url.count_words(
        text="Tell the audience what you're going to say. Say it. "
        "Then tell them what you've said."
    )
    assert_that(result, mapping_includes(expected_word_count))
