import pytest
from loguru import logger
from pydantic import HttpUrl

from word_counter_service.db import crud_jobs
from word_counter_service.serializers import JobStatus
from word_counter_service.tasks import process


@pytest.mark.asyncio
@logger.catch
async def test_process(async_db_session):
    async for db_session in async_db_session:
        http_url = HttpUrl("https://example.com", scheme="https")
        job = await crud_jobs.create(
            http_url=http_url, async_db_session=db_session
        )

        assert job.status == JobStatus.created
        assert job.http_url is str(http_url)
        assert job.error is None
        assert job.result is None
        original_job_id = job.id

        await process(
            http_url=http_url,
            job=job,
            async_db_session=db_session,
        )

        assert job.status == JobStatus.ready
        assert job.http_url is str(http_url)
        assert job.error is None
        assert job.result is not None
        assert original_job_id == job.id
