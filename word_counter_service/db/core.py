from asyncio import current_task
from typing import AsyncGenerator

from asyncpg import DuplicateDatabaseError
from databases import DatabaseURL
from loguru import logger
from sqlalchemy.engine import Connection
from sqlalchemy.exc import ProgrammingError
from sqlalchemy.ext.asyncio import (
    AsyncSession,
    async_scoped_session,
    create_async_engine,
)
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy_utils import create_database

from word_counter_service.settings import DATABASE_URL, DEBUG

async_engine = create_async_engine(
    str(DATABASE_URL), echo=DEBUG, pool_pre_ping=True
)
Base = declarative_base()


async def get_db_session() -> AsyncGenerator:
    async_session_factory = sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=async_engine,
        class_=AsyncSession,
    )

    AsyncScopedSession = async_scoped_session(
        async_session_factory, scopefunc=current_task
    )

    async with AsyncScopedSession() as local_session:
        try:
            yield local_session
        finally:
            await local_session.close()


@logger.catch
def create_database_sync(_connection: Connection):
    try:
        create_database(str(DATABASE_URL))
    except ProgrammingError as exception:

        def contexts(e):
            ctx = e.__context__
            if ctx is not None:
                yield ctx
                yield from contexts(ctx)

        if not any(
            isinstance(exc, DuplicateDatabaseError)
            for exc in contexts(exception)
        ):
            raise
    else:
        logger.info(f"Database {DATABASE_URL} has been created")


async def create_database_if_not_exists():
    # switch to default 'postgres' database to create a new one
    # assuming 'postgres' database is always present
    postgres_db_url = str(
        DatabaseURL(DATABASE_URL).replace(database="postgres")
    )

    async_engine = create_async_engine(postgres_db_url, echo=DEBUG)

    async with async_engine.connect() as connection:
        await connection.run_sync(create_database_sync)
    await async_engine.dispose()
