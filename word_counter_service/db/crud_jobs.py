__all__: list[str] = [
    "create",
    "update",
    "read",
]

from typing import AsyncGenerator

import sqlalchemy as sqla
from pydantic import HttpUrl
from sqlalchemy.ext.asyncio import AsyncSession

from word_counter_service import logger
from word_counter_service.models import Job
from word_counter_service.serializers import JobUpdate
from word_counter_service.settings import DEFAULT_PAGE_LIMIT


async def create(
    *, http_url: HttpUrl, async_db_session: AsyncGenerator | AsyncSession
) -> Job:
    logger.debug(f"Create job for {http_url}")
    job = Job(http_url=http_url)
    async_db_session.add(job)
    await async_db_session.commit()
    await async_db_session.refresh(job)
    return job


async def read(
    *, job_id: int, async_db_session: AsyncGenerator | AsyncSession
) -> Job:
    logger.debug(f"Read job with id={job_id}")
    statement = sqla.select(Job).where(Job.id == job_id)
    result = await async_db_session.execute(statement)
    job = result.scalars().first()
    return job


async def select_all(
    *,
    limit: int = DEFAULT_PAGE_LIMIT,
    offset: int = 0,
    async_db_session: AsyncGenerator | AsyncSession,
) -> Job:
    logger.debug(f"List jobs with limit={limit} offset={offset}")
    statement = sqla.select(Job).limit(limit).offset(offset)
    result = await async_db_session.execute(statement)
    jobs = result.scalars().all()
    return jobs


async def update(
    *, job_update: JobUpdate, async_db_session: AsyncGenerator | AsyncSession
) -> Job:
    logger.debug(f"Update job with {job_update} using {async_db_session}")

    values = job_update.dict(exclude_unset=True, exclude={"id": True})
    statement = (
        sqla.update(Job)
        .where(Job.id == job_update.id)
        .values(**values)
        .returning(Job)
    )
    # ORM specific construct. No SELECT will be emitted,
    # update.returning() did the work in atomic operation.
    orm_update = (
        sqla.select(Job)
        .from_statement(statement)
        .execution_options(populate_existing=True)
    )
    result = await async_db_session.execute(orm_update)
    await async_db_session.commit()
    updated_job = result.first()
    return updated_job
