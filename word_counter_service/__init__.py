from loguru import logger

from word_counter_service.settings import DEBUG

if DEBUG:
    logger.level = "DEBUG"
