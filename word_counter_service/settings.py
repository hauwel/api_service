import uuid

import databases
from starlette.config import Config

__all__: list[str] = [
    "config",
    "DEBUG",
    "DATABASE_URL",
    "BEAUTIFULSOUP_PARSER",
    "NON_WORD_REGEXP",
    "DEFAULT_PAGE_LIMIT",
    "TEST",
]
config = Config("config.env")

TEST = config("TEST", cast=bool, default=False)
DEBUG = config("DEBUG", cast=bool, default=False)
DATABASE_URL = config("DATABASE_URL", cast=databases.DatabaseURL)
BEAUTIFULSOUP_PARSER = config("BEAUTIFULSOUP_PARSER", default=None)
NON_WORD_REGEXP = config("NON_WORD_REGEXP", default="[^-_a-zA-Z0-9']+")
DEFAULT_PAGE_LIMIT = config("DEFAULT_PAGE_LIMIT", default=42)

if TEST:
    test_run_id = uuid.uuid4().hex
    DATABASE_URL = DATABASE_URL.replace(
        database=f"test_{test_run_id}_{DATABASE_URL.database}"
    )
