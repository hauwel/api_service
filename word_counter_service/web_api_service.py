import http

from fastapi import BackgroundTasks, Body, Depends, FastAPI, HTTPException
from prometheus_fastapi_instrumentator import Instrumentator
from pydantic import HttpUrl
from sqlalchemy.ext.asyncio import AsyncSession

from word_counter_service import serializers
from word_counter_service.db import crud_jobs, get_db_session
from word_counter_service.settings import DEBUG, DEFAULT_PAGE_LIMIT
from word_counter_service.tasks.process_http_url import process

tags_metadata = [
    {
        "name": "Prometheus Monitoring",
        "description": "Special endpoint providing "
        "metrics for prometheus monitoring",
    },
    {
        "name": "Word Count Application",
        "description": "These App is operating jobs objects to "
        "create and then read status of a word counting job.",
    },
]

app = FastAPI(tags_metadata=tags_metadata, debug=DEBUG)

# Monitoring Endpoint
Instrumentator().instrument(app).expose(app, tags=["Prometheus Monitoring"])


@app.get("/", include_in_schema=False)
async def index():
    return "Welcome to the Word Count Web API"


# Jobs APIs
TAGS: list[str] = ["Word Count Application"]


@app.post(
    "/jobs",
    tags=TAGS,
    response_model=serializers.Job,
    status_code=http.HTTPStatus.CREATED,
)
async def create_job_for_background_processing(
    background_tasks: BackgroundTasks,
    http_url: HttpUrl = Body(
        ..., embed=True, example={"http_url": "https://example.com"}
    ),
    async_db_session: AsyncSession = Depends(get_db_session),
):
    """Sends a URL for processing.

    It creates a background task. A response will contain Job resource
    description. The Job might not be completed yet so a client can poll
    a Job resource URL for status updates and results.

    - **http_url**: valid HTTP URL.
    \f
    :param http_url: User input.
    :param background_tasks: FastAPI background tasks feature.
    :param async_db_session: SQLAlchemy AsyncSession.

    """
    job = await crud_jobs.create(
        http_url=http_url, async_db_session=async_db_session
    )
    background_tasks.add_task(
        process, http_url=http_url, job=job, async_db_session=async_db_session
    )
    return job


@app.get("/jobs/", tags=TAGS, response_model=list[serializers.Job])
async def get_list_of_all_jobs(
    offset: int = 0,
    limit: int = DEFAULT_PAGE_LIMIT,
    async_db_session: AsyncSession = Depends(get_db_session),
):
    """Fetch all jobs.

    Simple operation to extract all jobs from DB. Limit and

    - **limit**: how much jobs to return. Default is configurable.
    - **offset**: starting index of the jobs list.
    \f
    :param limit: how much jobs to return.
    :param offset: starting index of the jobs list.
    :param async_db_session: SQLAlchemy AsyncSession.

    """

    jobs = await crud_jobs.select_all(
        limit=limit, offset=offset, async_db_session=async_db_session
    )
    return jobs


@app.get("/jobs/{job_id}/", tags=TAGS, response_model=serializers.Job)
async def get_job_representation_by_id(
    job_id: int,
    async_db_session: AsyncSession = Depends(get_db_session),
):
    """Get a job by its id.

    - **job_id**: the id to search for.
    \f
    :param job_id: the id to search for.
    :param async_db_session: SQLAlchemy AsyncSession.

    """

    job = await crud_jobs.read(
        job_id=job_id, async_db_session=async_db_session
    )
    if not job:
        raise HTTPException(status_code=404, detail="Job not found")
    return job
