from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import JSONB

from word_counter_service.db import Base
from word_counter_service.serializers import JobStatus


class Job(Base):
    __tablename__ = "jobs"
    __mapper_args__ = {"eager_defaults": True}

    id = Column(Integer, primary_key=True, index=True)
    status = Column(String, index=True, default=JobStatus.created)
    error = Column(String)
    result = Column(JSONB)
    http_url = Column(String, nullable=False)
