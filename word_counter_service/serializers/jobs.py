from enum import Enum

from pydantic import BaseModel, HttpUrl

__all__: list[str] = [
    "Job",
    "JobStatus",
    "JobErrors",
    "JobUpdate",
]


class JobStatus(str, Enum):
    created = "created"
    in_progress = "in_progress"
    ready = "ready"
    error = "error"


class JobErrors(str, Enum):
    url_not_found = "url_not_found"


class Job(BaseModel):
    id: int
    http_url: HttpUrl
    status: JobStatus = JobStatus.created
    error: str | None = None
    result: str | None = None

    class Config:
        use_enum_values = True
        orm_mode = True


class JobUpdate(Job):
    http_url: HttpUrl | None = None
    status: JobStatus | None = None
